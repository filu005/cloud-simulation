#pragma once

// screen space constants
namespace c
{
	auto const screenWidth = 800u;
	auto const screenHeight = 600u;

	const float aspectRatio = static_cast<float>(screenWidth) / static_cast<float>(screenHeight);
	const float viewHeight = 1.2f;
	const float viewWidth = aspectRatio * viewHeight;// 1.6
}

// grid constants
namespace c
{
	// tylko potegi 2 (bo powstaja niedokladnosci przy dzieleniu m.in. przy dx/dy/dz)
	auto const K = 64u;
	auto const L = 64u;
	auto const M = 64u;
	auto const KL = static_cast<long>(K) * static_cast<long>(L);
	auto const KLM = KL * static_cast<long>(M);

	// zmieniajac rozmiar boxa trzeba pamietac o zmianie skali
	// w shaderze color_cube_framebuffer.vert
	auto const xmin = -4.0f, ymin = -4.0f, zmin = -4.0f;
	auto const xmax = 4.0f, ymax = 4.0f, zmax = 4.0f;
	const float dx = (xmax - xmin) / static_cast<float>(K);
	const float dy = (ymax - ymin) / static_cast<float>(L);
	const float dz = (zmax - zmin) / static_cast<float>(M);
}

// simulation constants
namespace c
{
	auto const viscocity_ratio = 0.8f; //1e-3f
	auto const coefficient_pressure = 0.2f;
	auto const coefficient_vapor = 0.2f; // 2.4e-5 m^2/s
	auto const coefficient_temperature = 0.02f; // 3.73e-5f cal/cm s K; heat diffusion
	auto const coefficient_buoyancy = 0.12f; // 0.12f, dragging force
	auto const phase_transition_rate = 0.02f;// 0.5e-5f;
	auto const latent_heat = 0.2f;// 2270.0e-2f J/kg; https://en.wikipedia.org/wiki/Latent_heat
}