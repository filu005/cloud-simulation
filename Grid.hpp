#pragma once
#include <array>
#include <vector>

#include "constants.hpp"
#include "Paintable.hpp"

class Simulation;

struct Point
{
	Point() : velocity(0.0f), vapor(0.0f), droplets(0.0f), temperature(0.0f)
	{ }
	Point(glm::vec3 vel, float v, float d, float t) : velocity(vel), vapor(v), droplets(d), temperature(t)
	{ }

	Point operator+(Point const & a) const
	{
		return Point(a.velocity + this->velocity, a.vapor + this->vapor, a.droplets + this->droplets, a.temperature + this->temperature);
	}
	Point operator-(Point const & a) const
	{
		return Point(a.velocity - this->velocity, a.vapor - this->vapor, a.droplets - this->droplets, a.temperature - this->temperature);
	}
	Point operator*(float a)
	{
		return Point(a * this->velocity, a * this->vapor, a * this->droplets, a * this->temperature);
	}

	glm::vec3 velocity;
	float vapor;
	float droplets;
	float temperature;
};

class Grid : public Paintable
{
public:
	// �eby mo�na sobie maca� bezpo�rednio po siatce
	friend Simulation;

	Grid();

	Point const & operator()(size_t x, size_t y, size_t z) const
	{
		auto const idx = (static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL));
		return grid[idx];
	}

	Point & operator()(size_t x, size_t y, size_t z)
	{
		auto const idx = (static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL));
		return grid[idx];
	}

	void paint(Painter& p) const override final;
	void setup_buffers() override final;

	GLuint const get_density_texture() const { return density_texture; }
	void update_buffers();
	void clear_grid();

	GLsizei const bin_count = c::KLM;

private:
	// Hot stuff
	std::vector<Point> grid;// grid of all cells (containing all Points)

	// Geometry, instance offset array
	GLfloat const static square_vertices[6 * 2];
	std::array<glm::vec2, c::KLM> translations;

	// OpenGL
	GLuint instance_VBO;
	GLuint density_texture;
};
