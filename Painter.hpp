#pragma once
#include "Shader.hpp"

class Grid;
class VolumeRaycaster;

class Camera;

class Painter
{
public:
	Painter();

	void paint(Grid const & grid);
	void paint(VolumeRaycaster const & vr);

	void set_camera(Camera const & camera) { camera_ref = &camera; }

private:
	void paint_to_framebuffer(VolumeRaycaster const & vr);

	Shader grid_shader;
	Shader raycaster_shader;
	Shader color_cube_framebuffer_shader;

	Camera const * camera_ref;// Reference from Application class
};
