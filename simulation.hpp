#pragma once
#include <memory>

#include "Grid.hpp"
#include "VolumeRaycaster.hpp"

class Simulation
{
public:
	Simulation();

	void run();

	void generate_volume_from_noise();
	void set_noslip_boundary();
	void add_water_vapor();
	void viscosity_and_pressure_effect();
	void advection();
	void diffusion_of_water_vapor();
	void thermal_diffusion();
	void buoyancy();
	void phase_transition();

	void grad_test();

	// main components and also Paintables
	Grid grid;
	VolumeRaycaster volume_raycaster;

private:
	template <typename Container_T, typename Data_T>
		void set_boundary(Container_T & grid);
	
	template <typename Container_T, typename Data_T>
		void set_periodic_boundary(Container_T & grid);

	void set_velocity_boundary(std::vector<glm::vec3> & grid);
};