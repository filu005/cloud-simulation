#version 330 core
uniform sampler3D volume_texture;
uniform sampler2D front_volume_texture;
uniform sampler2D back_volume_texture;
uniform vec3 camera_position;
uniform mat4 model;

in vec4 projected_pos_coords;

out vec4 color;

void main()
{
	vec3 g_lightPos = vec3(3.0, 2.5, 2.5);
	vec3 g_lightIntensity = vec3(4.2, 4.2, 4.2);
	float g_absorption = 0.75;
	// diagonal of the cube
	const float maxDist = sqrt(3.0);

	// increases the number of steps for a single ray
	const float sampling_quality = 1.0/4.0;

	const int numSamples = 128 * int(1.0/sampling_quality);
	const float scale =  maxDist/float(numSamples);

	const int numLightSamples = 32;
	const float lscale = maxDist / float(numLightSamples);

	//calculate projective cube's texture coordinates
	//used to project the front and back position textures onto the cube
	vec2 projected_cube_coords = vec2(((projected_pos_coords.x / projected_pos_coords.w) + 1.0 ) / 2.0,
									  ((projected_pos_coords.y / projected_pos_coords.w) + 1.0 ) / 2.0 );

	// sample front and back lookup textures
	// they look like this: http://www.voreen.org//files/ray_entry_exit.jpg
	vec3 front = texture(front_volume_texture, projected_cube_coords).xyz;
	vec3 back = texture(back_volume_texture, projected_cube_coords).xyz;

	//get entry position and ray's direction
	vec3 pos = front;
	vec3 dir = normalize(back - front)*scale;

	// transmittance
	float T = 1.0;
	// in-scattered radiance
	vec3 Lo = vec3(0.0);

	for (int i=0; i < numSamples; ++i)
	{
		// If you want a texture access to return the exact value stored
		// at a voxel despite using GL_LINEAR you can make sure that your
		// texture coordinates fall at the center of a voxel. For example
		// to sample the value at voxel location (ix, iy, iz) from a texture
		// of size (sx, sy, sz) use texture coordinates
		// ((ix+0.5)/sx), (iy+0.5)/sy, (iz+0.5)/sz)), where ix,iy,iz is
		// the zero based integer location of the voxel.


		vec3 pos_mod = vec3((pos.x + 0.5 * (1.0/64.0)), (pos.y + 0.5 * (1.0/64.0)), (pos.z + 0.5 * (1.0/64.0)));
		// sample density
		float density = texture(volume_texture, pos_mod).a;

		// skip empty space
		if (density > 0.0)
		{
			// attenuate ray-throughput
			T *= 1.0-density*scale*g_absorption;
			if (T <= 0.01)
				break;

			// point light step_dir in texture space
			vec3 lightDir = normalize(g_lightPos-pos_mod)*lscale;

			// sample light
			float Tl = 1.0; // transmittance along light ray
			vec3 lpos = pos_mod + lightDir;

			for (int s=0; s < numLightSamples; ++s)
			{
				float ld = texture(volume_texture, lpos).a;
				Tl *= 1.0-g_absorption*ld*lscale;

				if (Tl <= 0.01)
					break;

				lpos += lightDir;
			}

			vec3 Li = g_lightIntensity*Tl;

			Lo += Li*T*scale*density;
		}

		// advance the current position
		pos += dir;
	}

	color.rgb = Lo;//Lo;
	color.a = 1.0-T;
}