#pragma once
#include <memory>
#include <array>
#include <utility>

#include "Grid.hpp"
#include "Paintable.hpp"

class VolumeRaycaster : public Paintable
{
public:
	VolumeRaycaster();
	~VolumeRaycaster();

	void paint(Painter& p) const override final;
	void setup_buffers() override final;

	// na razie przekazuje wszystkie dane z siatki
	// docelowo przekzywane b�d� tylko dane dotycz�ce g�sto�ci chmur
	void update_volume_data(std::vector<Point> const & volume_data);

	GLuint const get_volume_texture() const { return volume_texture; }
	std::pair<GLuint, GLuint> const get_front_back_color_cube_textures() const
	{
		return std::make_pair(front_volume_texture, back_volume_texture);
	}
	GLuint const get_front_back_volume_FBO() const { return front_back_volume_FBO; }

private:
	GLuint generate_voxel_texture3d();
	std::pair<GLuint, GLuint> const generate_front_back_color_cube_textures();

	// opengl
	GLuint EBO;
	GLuint volume_texture;
	GLuint front_back_volume_FBO;
	GLuint front_volume_texture, back_volume_texture;
	GLuint depth_volume_RBO;

	// data
	GLfloat const static box_data[108];
	GLfloat const static box_vertices[8 * 3];
	GLuint const static box_indices[36];
};
