app: application.o grid.o main.o painter.o simulation.o constants.hpp volumeraycaster.o
	g++ -std=gnu++11 -O3 application.o grid.o main.o painter.o simulation.o volumeraycaster.o -o app.out -L/usr/local/lib -lglfw -pthread -lGLEW -lGLU -lGL -lrt -lXrandr -lXxf86vm -lXi -lXinerama -lX11

application.o: Application.cpp Application.hpp
	g++ -std=gnu++11 -O3 Application.cpp -c -o application.o -lglut -lGL
grid.o: Grid.cpp Grid.hpp
	g++ -std=gnu++11 -O3 Grid.cpp -c -o grid.o -lglut -lGL
main.o: main.cpp
	g++ -std=gnu++11 -O3 main.cpp -c -o main.o -lglut -lGL
painter.o: Painter.cpp Painter.hpp
	g++ -std=gnu++11 -O3 Painter.cpp -c -o painter.o -lglut -lGL
simulation.o: simulation.cpp simulation.hpp
	g++ -std=gnu++11 -O3 simulation.cpp -c -o simulation.o -lglut -lGL
volumeraycaster.o: VolumeRaycaster.cpp VolumeRaycaster.hpp
	g++ -std=gnu++11 -O3 VolumeRaycaster.cpp -c -o volumeraycaster.o -lglut -lGL

clean:
	rm -f *.o app.out
