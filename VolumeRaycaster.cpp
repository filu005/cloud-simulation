#include <vector>
#include "Painter.hpp"
#include "VolumeRaycaster.hpp"


VolumeRaycaster::VolumeRaycaster()
{
	setup_buffers();
}

VolumeRaycaster::~VolumeRaycaster()
{
	glDeleteTextures(1, &volume_texture);
	glDeleteRenderbuffers(1, &depth_volume_RBO);
	glDeleteTextures(1, &front_volume_texture);
	glDeleteTextures(1, &back_volume_texture);
	glDeleteFramebuffers(1, &front_back_volume_FBO);
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}

void VolumeRaycaster::paint(Painter& p) const
{
	p.paint(*this);
}

void VolumeRaycaster::setup_buffers(void)
{
	// Create buffers/arrays
	glGenVertexArrays(1, &this->VAO);
	glBindVertexArray(this->VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(this->box_vertices), &box_vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &this->EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(this->box_indices), &this->box_indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*) 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// create 3D texture for data
	volume_texture = generate_voxel_texture3d();

	glBindVertexArray(0);

	// Create Framebuffer for two textures used in volume rendering-raycasting shader
	glGenFramebuffers(1, &front_back_volume_FBO);
	glBindFramebuffer(GL_FRAMEBUFFER, front_back_volume_FBO);

	// create a renderbuffer object to store depth info
	glGenRenderbuffers(1, &depth_volume_RBO);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_volume_RBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, c::screenWidth, c::screenHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// attach the renderbuffer to depth attachment point:
	glFramebufferRenderbuffer(GL_FRAMEBUFFER,      // 1. fbo target: GL_FRAMEBUFFER
		GL_DEPTH_ATTACHMENT, // 2. attachment point
		GL_RENDERBUFFER,     // 3. rbo target: GL_RENDERBUFFER
		depth_volume_RBO);   // 4. rbo ID

	// Create a color attachment texture:
	std::tie(front_volume_texture, back_volume_texture) = generate_front_back_color_cube_textures();
	// attach the texture to FBO color attachment point:
	//glFramebufferTexture2D(GL_FRAMEBUFFER,        // 1. fbo target: GL_FRAMEBUFFER 
	//	GL_COLOR_ATTACHMENT0,  // 2. attachment point
	//	GL_TEXTURE_2D,         // 3. tex target: GL_TEXTURE_2D
	//	textureId,             // 4. tex ID
	//	0);                    // 5. mipmap level: 0(base)
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, front_volume_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, back_volume_texture, 0);


	// Now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void VolumeRaycaster::update_volume_data(std::vector<Point> const & volume_data)
{
	std::vector<GLfloat> rgba_data;
	rgba_data.resize(c::KLM * 4);

	for(size_t i = 0; i < c::KLM; ++i)
	{
		rgba_data[i * 4] = 0.0f;
		rgba_data[i * 4 + 1] = 0.0f;
		rgba_data[i * 4 + 2] = 0.0f;
		rgba_data[i * 4 + 3] = volume_data[i].droplets;
		//std::cout << rgba_data[i * 4 + 3] << "\n";
	}
	
	// update 3D texture
	glBindTexture(GL_TEXTURE_3D, this->volume_texture);
	glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0, c::K, c::L, c::M, GL_RGBA, GL_FLOAT, &rgba_data[0]);
	glBindTexture(GL_TEXTURE_3D, 0);
}


GLuint VolumeRaycaster::generate_voxel_texture3d()
{
	GLuint texid;
	glGenTextures(1, &texid);

	GLenum target = GL_TEXTURE_3D;
	GLenum filter = GL_LINEAR;
	GLenum address = GL_CLAMP_TO_BORDER;
	GLfloat color[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	glBindTexture(target, texid);

	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);

	glTexParameteri(target, GL_TEXTURE_WRAP_S, address);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, address);
	glTexParameteri(target, GL_TEXTURE_WRAP_R, address);
	glTexParameterfv(target, GL_TEXTURE_BORDER_COLOR, color);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	glTexImage3D(target,
		0,
		GL_RGBA8,
		c::K,
		c::L,
		c::M,
		0,
		GL_RGBA,
		GL_FLOAT,
		NULL);

	glBindTexture(target, 0);

	return texid;
}

std::pair<GLuint, GLuint> const VolumeRaycaster::generate_front_back_color_cube_textures()
{
	std::array<GLuint, 2> tex_front_back;

	glGenTextures(2, &tex_front_back[0]);

	for(auto const tex : tex_front_back)
	{
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, c::screenWidth, c::screenHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	// wspolczynnik skalowania wspolrzednych tekstury one_volBoundingBox_ratio w shaderze!!!
	// inaczej tekstury beda rozciagane przez GL_CLAMP_TO_EDGE: https://open.gl/textures

	return std::make_pair(tex_front_back[0], tex_front_back[1]);
}

/*
Remember: to specify vertices in a counter-clockwise winding order you need to visualize the triangle
as if you're in front of the triangle and from that point of view, is where you set their order.

To define the order of a triangle on the right side of the cube for example, you'd imagine yourself looking
straight at the right side of the cube, and then visualize the triangle and make sure their order is specified
in a counter-clockwise order. This takes some practice, but try visualizing this yourself and see that this
is correct.
*/

GLfloat const VolumeRaycaster::box_data[] =
{
	// Back face
	-1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	// Front face
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	// Left face
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	// Right face
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	// Bottom face
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,
	// Top face
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, -1.0f,
	1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f, 1.0f
};

GLfloat const VolumeRaycaster::box_vertices[] =
{
	//front
	c::xmin, c::ymin, c::zmax,
	c::xmax, c::ymin, c::zmax,
	c::xmax, c::ymax, c::zmax,
	c::xmin, c::ymax, c::zmax,
	//back
	c::xmin, c::ymin, c::zmin,
	c::xmax, c::ymin, c::zmin,
	c::xmax, c::ymax, c::zmin,
	c::xmin, c::ymax, c::zmin
};

GLuint const VolumeRaycaster::box_indices[] =
{
	// Front
	0, 1, 2, 2, 3, 0,
	// Back
	4, 7, 6, 6, 5, 4,
	// Right
	5, 6, 2, 2, 1, 5,
	// Left
	4, 0, 3, 3, 7, 4,
	// Top
	7, 3, 2, 2, 6, 7,
	// Bottom
	4, 5, 1, 1, 0, 4
};