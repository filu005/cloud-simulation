// GL Includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Grid.hpp"
#include "VolumeRaycaster.hpp"
#include "Camera.hpp"
#include "Painter.hpp"

Painter::Painter() : grid_shader("./shaders/grid_2d.vert", "./shaders/grid_2d.frag"),
	raycaster_shader("./shaders/raycaster.vert", "./shaders/raycaster.frag"),
	color_cube_framebuffer_shader("./shaders/color_cube_framebuffer.vert", "./shaders/color_cube_framebuffer.frag"),
	camera_ref(nullptr)
{

}

void Painter::paint(Grid const & grid)
{
	grid_shader.Use();

	// Create transformations
	glm::mat4 view;
	glm::mat4 model;
	glm::mat4 projection;
	assert(camera_ref != nullptr);
	auto const camera = *camera_ref;

	view = camera.GetViewMatrix();
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
	projection = glm::perspective(camera.Zoom, c::aspectRatio, 0.1f, 1000.0f);

	// Get their uniform location
	GLint viewLoc = glGetUniformLocation(grid_shader.Program, "view");
	GLint modelLoc = glGetUniformLocation(grid_shader.Program, "model");
	GLint projLoc = glGetUniformLocation(grid_shader.Program, "projection");
	// Pass them to the shaders
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, grid.get_density_texture());
	glUniform1i(glGetUniformLocation(grid_shader.Program, "density_texture"), 0);

	auto const & VAO = grid.getVAO();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBindVertexArray(VAO);
	glDrawArraysInstanced(GL_TRIANGLES, 0, 6 * 2, grid.bin_count);
	glBindVertexArray(0);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void Painter::paint(VolumeRaycaster const & vr)
{
	paint_to_framebuffer(vr);
	raycaster_shader.Use();

	GLuint front_volume_texture, back_volume_texture;
	std::tie(front_volume_texture, back_volume_texture) = vr.get_front_back_color_cube_textures();

	// Create transformations
	glm::mat4 view;
	glm::mat4 model;
	glm::mat4 projection;
	assert(camera_ref != nullptr);
	auto const camera = *camera_ref;

	view = camera.GetViewMatrix();
	projection = glm::perspective(camera.Zoom, c::aspectRatio, 0.1f, 1000.0f);

	// Get their uniform location
	GLint viewLoc = glGetUniformLocation(raycaster_shader.Program, "view");
	GLint modelLoc = glGetUniformLocation(raycaster_shader.Program, "model");
	GLint projLoc = glGetUniformLocation(raycaster_shader.Program, "projection");
	GLint cameraPosLoc = glGetUniformLocation(raycaster_shader.Program, "camera_position");
	// Pass them to the shaders
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
	glUniform3fv(cameraPosLoc, 1, &camera.Position[0]);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, vr.get_volume_texture());
	glUniform1i(glGetUniformLocation(raycaster_shader.Program, "volume_texture"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, front_volume_texture);
	glUniform1i(glGetUniformLocation(raycaster_shader.Program, "front_volume_texture"), 1);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, back_volume_texture);
	glUniform1i(glGetUniformLocation(raycaster_shader.Program, "back_volume_texture"), 2);

	auto const & VAO = vr.getVAO();

	glEnable(GL_CULL_FACE);

	// Draw cube
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glDisable(GL_CULL_FACE);
}

void Painter::paint_to_framebuffer(VolumeRaycaster const & vr)
{
	GLuint const front_back_volume_FBO = vr.get_front_back_volume_FBO();
	GLuint front_volume_texture, back_volume_texture;
	std::tie(front_volume_texture, back_volume_texture) = vr.get_front_back_color_cube_textures();

	glBindFramebuffer(GL_FRAMEBUFFER, front_back_volume_FBO);
	color_cube_framebuffer_shader.Use();

	//glEnable(GL_DEPTH_TEST);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 view;
	glm::mat4 model;
	glm::mat4 projection;
	assert(camera_ref != nullptr);
	auto const camera = *camera_ref;

	view = camera.GetViewMatrix();
	projection = glm::perspective(camera.Zoom, c::aspectRatio, 0.1f, 1000.0f);

	// Get their uniform location
	GLint viewLoc = glGetUniformLocation(color_cube_framebuffer_shader.Program, "view");
	GLint modelLoc = glGetUniformLocation(color_cube_framebuffer_shader.Program, "model");
	GLint projLoc = glGetUniformLocation(color_cube_framebuffer_shader.Program, "projection");
	// Pass them to the shaders
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

	auto const & VAO = vr.getVAO();

	// Draw cube
	//GLenum buffer1[] = { GL_COLOR_ATTACHMENT0, GL_NONE };
	GLenum buffer1[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, buffer1);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindVertexArray(VAO);
	glBindTexture(GL_TEXTURE_2D, front_volume_texture);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	// Draw reverse cube - it's back faces
	// this dual buffer setup works for win7, win8.1 AMD and Nvidia drivers
	//GLenum buffer2[] = { GL_NONE, GL_COLOR_ATTACHMENT1 };
	// this works for win10 AMD and Intel drivers
	GLenum buffer2[] = { GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(1, buffer2);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glBindVertexArray(VAO);
	glBindTexture(GL_TEXTURE_2D, back_volume_texture);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindVertexArray(0);

	glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}