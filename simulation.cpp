#include <vector>
#include <iostream>
#include <ctime>
#include <noise/noise.h>
#include "simulation.hpp"

Simulation::Simulation()
{
	for(int i = 0; i < c::M; ++i)
	{
		for(int j = 0; j < c::L; ++j)
		{
			for(int k = 0; k < c::K; ++k)
			{
				// (...) we set the lattice size to 20[m].
				// As for the temperature distribution, the temperature at the surface of the earth is considered to
				// be 300[K], and the temperature falls by 0.6[K] for each 100[m] step in altitude.
				auto mod = 300.0f - (0.6f * static_cast<float>(j) * (20.0f / 100.0f));// static_cast<float>(c::M - i) / static_cast<float>(c::M);
				grid(k, j, i).temperature = mod;
				grid(k, j, i).velocity = glm::vec3(0.0f, 0.01f, 0.0f);
			}
		}
	}
	//add_water_vapor();
}

void Simulation::run()
{
	// kolejno�� wed�ug:
	// http://chaos.c.u-tokyo.ac.jp/papers/pr/yanagita97.pdf

	//buoyancy();
	//add_water_vapor();
	//viscosity_and_pressure_effect();
	//thermal_diffusion();
	//diffusion_of_water_vapor();
	//phase_transition();
	//advection();

	//grad_test();

	// generator perlin noise'a 3D:
	generate_volume_from_noise();

	// do 2d
	//grid.update_buffers();

	// do raycastingu 3d
	volume_raycaster.update_volume_data(grid.grid);
}

void Simulation::generate_volume_from_noise()
{
	using namespace noise;
	module::Perlin perlin;
	perlin.SetSeed(12342);//albo seed jako: time(0)
	float frequency = 3.0f / static_cast<float>(c::K);
	perlin.SetFrequency(frequency);

	float center = static_cast<float>(c::K) / 2.0f + 0.5f;
	auto const r = 0.25f;
	auto const A = 0.35f;

	for(int z = 0; z < c::M; z++)
	{
		for(int y = 0; y < c::L; ++y)
		{
			for(int x = 0; x < c::K; ++x)
			{
				float dx = center - x;
				float dy = center - y;
				float dz = center - z;

				//float off = (perlin_noise_gen->generate(x*frequency, y*frequency, z*frequency));
				float off = (static_cast<float>(perlin.GetValue(x, y, z)));

				float d = sqrtf(dx*dx + dy*dy + dz*dz) / c::K;

				//grid(x, y, z).droplets = ((d - off) < r) ? 255.0f : 0.0f;
				grid(x, y, z).droplets = (A*(d - r)*off) * 255.0f;
			}
		}
	}
}

void Simulation::add_water_vapor()
{
	for(int z = static_cast<int>(c::M * 0.45f); z < static_cast<int>(c::M * 0.55f); ++z)
	{
		for(int x = static_cast<int>(c::K * 0.45f); x < static_cast<int>(c::K * 0.55f); ++x)
		{
			grid(x, 0, z).vapor += 10000.0f;
			//grid(x, 0, z).velocity.y += 0.01f;
		}
	}
}

void Simulation::set_noslip_boundary()
{
	// na brzegach
	for(int z = 0; z < c::M; z += (c::M - 1))
	{
		for(int y = 0; y < c::L; y += (c::L - 1))
		{
			for(int x = 0; x < c::K; x += (c::K - 1))
			{
				grid(x, y, z).velocity = glm::vec3(0.0f);
			}
		}
	}
}

template <typename Container_T, typename Data_T>
void Simulation::set_boundary(Container_T & grid)
{
	auto grid_point = [=, &grid](int x, int y, int z) -> Data_T&
	{
		return grid[(static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL))];
	};

	// no-flux conditions

	// YZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int y = 0; y < c::L; ++y)
		{
			auto x = 0;
			grid_point(x, y, z) = grid_point(x + 1, y, z);
			x = c::K - 1;
			grid_point(x, y, z) = grid_point(x - 1, y, z);
		}
	}

	// XY
	for(int y = 0; y < c::L; ++y)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto z = 0;
			grid_point(x, y, z) = grid_point(x, y, z + 1);
			z = c::M - 1;
			grid_point(x, y, z) = grid_point(x, y, z - 1);
		}
	}

	// XZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto y = 0;
			// y = 0 to dolna powierzchnia, ktorej warunki brzegowe ustalamy inaczej
			// (jako stale zrodla parowania)
			grid_point(x, y, z) = grid_point(x, y + 1, z);
			y = c::L - 1;
			grid_point(x, y, z) = grid_point(x, y - 1, z);
		}
	}

	// rogi
	grid_point(0, 0, 0) = (grid_point(1, 0, 0) + grid_point(0, 1, 0) + grid_point(0, 0, 1))*0.33f;
	grid_point(c::K - 1, 0, 0) = (grid_point(c::K - 2, 0, 0) + grid_point(c::K - 1, 1, 0) + grid_point(c::K - 1, 0, 1))*0.33f;
	grid_point(0, c::L - 1, 0) = (grid_point(1, c::L - 1, 0) + grid_point(0, c::L - 2, 0) + grid_point(0, c::L - 1, 1))*0.33f;
	grid_point(0, 0, c::M - 1) = (grid_point(1, 0, c::M - 1) + grid_point(0, 1, c::M - 1) + grid_point(0, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, 0) = (grid_point(c::K - 2, c::L - 1, 0) + grid_point(c::K - 1, c::L - 2, 0) + grid_point(c::K - 1, c::L - 1, 1))*0.33f;
	grid_point(0, c::L - 1, c::M - 1) = (grid_point(1, c::L - 1, c::M - 1) + grid_point(0, c::L - 2, c::M - 1) + grid_point(0, c::L - 1, c::M - 2))*0.33f;
	grid_point(c::K - 1, 0, c::M - 1) = (grid_point(c::K - 2, 0, c::M - 1) + grid_point(c::K - 1, 1, c::M - 1) + grid_point(c::K - 1, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, c::M - 1) = (grid_point(c::K - 2, c::L - 1, c::M - 1) + grid_point(c::K - 1, c::L - 2, c::M - 1) + grid_point(c::K - 1, c::L - 1, c::M - 2))*0.33f;
}


void Simulation::set_velocity_boundary(std::vector<glm::vec3> & grid)
{
	auto grid_point = [=, &grid](int x, int y, int z) -> glm::vec3&
	{
		return grid[(static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL))];
	};

	// no-flux conditions

	// YZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int y = 0; y < c::L; ++y)
		{
			auto x = 0;
			grid_point(x, y, z).x = -grid_point(x + 1, y, z).x;
			x = c::K - 1;
			grid_point(x, y, z).x = -grid_point(x - 1, y, z).x;
		}
	}

	// XY
	for(int y = 0; y < c::L; ++y)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto z = 0;
			grid_point(x, y, z).z = -grid_point(x, y, z + 1).z;
			z = c::M - 1;
			grid_point(x, y, z).z = -grid_point(x, y, z - 1).z;
		}
	}

	// XZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto y = 0;
			// y = 0 to dolna powierzchnia, ktorej warunki brzegowe ustalamy inaczej
			// (jako stale zrodla parowania)
			grid_point(x, y, z).y = -grid_point(x, y + 1, z).y;
			y = c::L - 1;
			grid_point(x, y, z).y = -grid_point(x, y - 1, z).y;
		}
	}

	// rogi
	grid_point(0, 0, 0) = (grid_point(1, 0, 0) + grid_point(0, 1, 0) + grid_point(0, 0, 1))*0.33f;
	grid_point(c::K - 1, 0, 0) = (grid_point(c::K - 2, 0, 0) + grid_point(c::K - 1, 1, 0) + grid_point(c::K - 1, 0, 1))*0.33f;
	grid_point(0, c::L - 1, 0) = (grid_point(1, c::L - 1, 0) + grid_point(0, c::L - 2, 0) + grid_point(0, c::L - 1, 1))*0.33f;
	grid_point(0, 0, c::M - 1) = (grid_point(1, 0, c::M - 1) + grid_point(0, 1, c::M - 1) + grid_point(0, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, 0) = (grid_point(c::K - 2, c::L - 1, 0) + grid_point(c::K - 1, c::L - 2, 0) + grid_point(c::K - 1, c::L - 1, 1))*0.33f;
	grid_point(0, c::L - 1, c::M - 1) = (grid_point(1, c::L - 1, c::M - 1) + grid_point(0, c::L - 2, c::M - 1) + grid_point(0, c::L - 1, c::M - 2))*0.33f;
	grid_point(c::K - 1, 0, c::M - 1) = (grid_point(c::K - 2, 0, c::M - 1) + grid_point(c::K - 1, 1, c::M - 1) + grid_point(c::K - 1, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, c::M - 1) = (grid_point(c::K - 2, c::L - 1, c::M - 1) + grid_point(c::K - 1, c::L - 2, c::M - 1) + grid_point(c::K - 1, c::L - 1, c::M - 2))*0.33f;
}

template <typename Container_T, typename Data_T>
void Simulation::set_periodic_boundary(Container_T & grid)
{
	auto grid_point = [=, &grid](int x, int y, int z) -> Data_T&
	{
		return grid[(static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL))];
	};

	// YZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int y = 0; y < c::L; ++y)
		{
			auto x = 0;
			grid_point(x, y, z) = grid_point(c::K - 2, y, z);
			x = c::K - 1;
			grid_point(x, y, z) = grid_point(1, y, z);
		}
	}

	// XY
	for(int y = 0; y < c::L; ++y)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto z = 0;
			grid_point(x, y, z) = grid_point(x, y, c::M - 2);
			z = c::M - 1;
			grid_point(x, y, z) = grid_point(x, y, 1);
		}
	}

	// XZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto y = 0;
			// y = 0 to dolna powierzchnia, ktorej warunki brzegowe ustalamy inaczej
			// (jako stale zrodla parowania)
			grid_point(x, y, z) = grid_point(x, y + 1, z);
			y = c::L - 1;
			grid_point(x, y, z) = grid_point(x, y - 1, z);
		}
	}

	// rogi
	grid_point(0, 0, 0) = (grid_point(1, 0, 0) + grid_point(0, 1, 0) + grid_point(0, 0, 1))*0.33f;
	grid_point(c::K - 1, 0, 0) = (grid_point(c::K - 2, 0, 0) + grid_point(c::K - 1, 1, 0) + grid_point(c::K - 1, 0, 1))*0.33f;
	grid_point(0, c::L - 1, 0) = (grid_point(1, c::L - 1, 0) + grid_point(0, c::L - 2, 0) + grid_point(0, c::L - 1, 1))*0.33f;
	grid_point(0, 0, c::M - 1) = (grid_point(1, 0, c::M - 1) + grid_point(0, 1, c::M - 1) + grid_point(0, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, 0) = (grid_point(c::K - 2, c::L - 1, 0) + grid_point(c::K - 1, c::L - 2, 0) + grid_point(c::K - 1, c::L - 1, 1))*0.33f;
	grid_point(0, c::L - 1, c::M - 1) = (grid_point(1, c::L - 1, c::M - 1) + grid_point(0, c::L - 2, c::M - 1) + grid_point(0, c::L - 1, c::M - 2))*0.33f;
	grid_point(c::K - 1, 0, c::M - 1) = (grid_point(c::K - 2, 0, c::M - 1) + grid_point(c::K - 1, 1, c::M - 1) + grid_point(c::K - 1, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, c::M - 1) = (grid_point(c::K - 2, c::L - 1, c::M - 1) + grid_point(c::K - 1, c::L - 2, c::M - 1) + grid_point(c::K - 1, c::L - 1, c::M - 2))*0.33f;
}

void Simulation::viscosity_and_pressure_effect()
{
	auto velocity_grid = std::vector<glm::vec3>();
	velocity_grid.resize(c::KLM);
	
	for(long i = 0; i < c::KLM; ++i)
		velocity_grid[i] = grid.grid[i].velocity;


	auto velocity_grid_point = [=, &velocity_grid](int x, int y, int z) -> glm::vec3&
	{
		auto idx = (static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL));
		return velocity_grid[idx];
	};

	// debug vars
	auto sumx = 0.0f;
	auto sumy = 0.0f;
	auto sumz = 0.0f;

	for(int y = 1; y < (c::L - 1); ++y)
	{
		for(int x = 1; x < (c::K - 1); ++x)
		{
			for(int z = 1; z < (c::M - 1); ++z)
			{
				// eqation (2)
				auto part1 = grid(x, y, z).velocity;

				// equation (3)
				auto delta_velocity = (1.0f / 6.0f)*(grid(x + 1, y, z).velocity + grid(x - 1, y, z).velocity +
					grid(x, y + 1, z).velocity + grid(x, y - 1, z).velocity +
					grid(x, y, z + 1).velocity + grid(x, y, z - 1).velocity +
					(-6.0f)*grid(x, y, z).velocity);

				auto part2 = c::viscocity_ratio * delta_velocity;

				// equation (4): grad(div(v))
				auto grad_velocity_x = 0.5f * (grid(x + 1, y, z).velocity.x + grid(x - 1, y, z).velocity.x + (-2.0f) * grid(x, y, z).velocity.x) +
					0.25f * (grid(x + 1, y + 1, z).velocity.y + grid(x - 1, y - 1, z).velocity.y - grid(x - 1, y + 1, z).velocity.y -
					grid(x + 1, y - 1, z).velocity.y + grid(x + 1, y, z + 1).velocity.z + grid(x - 1, y, z - 1).velocity.z -
					grid(x - 1, y, z + 1).velocity.z - grid(x + 1, y, z - 1).velocity.z);

				auto grad_velocity_y = 0.5f * (grid(x, y + 1, z).velocity.y + grid(x, y - 1, z).velocity.y + (-2.0f) * grid(x, y, z).velocity.y) +
					0.25f * (grid(x + 1, y + 1, z).velocity.x + grid(x - 1, y - 1, z).velocity.x - grid(x - 1, y + 1, z).velocity.x -
					grid(x + 1, y - 1, z).velocity.x + grid(x, y + 1, z + 1).velocity.z + grid(x, y - 1, z - 1).velocity.z -
					grid(x, y - 1, z + 1).velocity.z - grid(x, y + 1, z - 1).velocity.z);

				auto grad_velocity_z = 0.5f * (grid(x, y, z + 1).velocity.z + grid(x, y, z - 1).velocity.z + (-2.0f) * grid(x, y, z).velocity.z) +
					0.25f * (grid(x + 1, y, z + 1).velocity.x + grid(x - 1, y, z - 1).velocity.x - grid(x - 1, y, z + 1).velocity.x -
					grid(x + 1, y, z - 1).velocity.x + grid(x, y + 1, z + 1).velocity.y + grid(x, y - 1, z - 1).velocity.y -
					grid(x, y - 1, z + 1).velocity.y - grid(x, y + 1, z - 1).velocity.y);

				auto part3 = glm::vec3(grad_velocity_x, grad_velocity_y, grad_velocity_z);
				part3 *= c::coefficient_pressure;

				auto new_velocity = glm::vec3(0.0f);
				new_velocity = part1 + part2 + part3;

				sumx += grad_velocity_x;
				sumy += grad_velocity_y;
				sumz += grad_velocity_z;

				//if(new_velocity.x >= 1.0f)
				//	new_velocity.x = 0.99f;
				//else if(new_velocity.x <= -1.0f)
				//	new_velocity.x = -0.99f;
				//if(new_velocity.y >= 1.0f)
				//	new_velocity.y = 0.99f;
				//else if(new_velocity.y <= -1.0f)
				//	new_velocity.y = -0.99f;
				//if(new_velocity.z >= 1.0f)
				//	new_velocity.z = 0.99f;
				//else if(new_velocity.z <= -1.0f)
				//	new_velocity.z = -0.99f;

				velocity_grid_point(x, y, z) = new_velocity;
			}
		}
	}

	set_velocity_boundary(velocity_grid);
	//std::cout << sumx << ",\t" << sumy << ",\t" << sumz << "\n";

	for(long i = 0; i < c::KLM; ++i)
		grid.grid[i].velocity = velocity_grid[i];

}

void Simulation::grad_test()
{
	auto sumx = 0.0f;
	auto sumy = 0.0f;
	auto sumz = 0.0f;

	for(int z = 1; z < (c::M - 1); ++z)
	{
		for(int y = 1; y < (c::L - 1); ++y)
		{
			for(int x = 1; x < (c::K - 1); ++x)
			{
				sumx += grid(x + 1, y + 1, z).velocity.y + grid(x - 1, y - 1, z).velocity.y -
					grid(x + 1, y - 1, z).velocity.y - grid(x - 1, y + 1, z).velocity.y;

				//sumy += grad_velocity_y;

				sumz += grid(x, y + 1, z + 1).velocity.y + grid(x, y - 1, z - 1).velocity.y -
					grid(x, y - 1, z + 1).velocity.y - grid(x, y + 1, z - 1).velocity.y;
			}
		}
	}

	//set_velocity_boundary(velocity_grid);
	//std::cout << sumx << ",\t" << sumy << ",\t" << sumz << "\n";
}

void Simulation::advection()
{
	auto advected_grid = std::vector<Point>();
	advected_grid.resize(c::KLM);

	auto advected_grid_point = [&](int x, int y, int z) -> Point&
	{
		return advected_grid[(static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL))];
	};

	for(long i = 0; i < c::KLM; ++i)
		advected_grid[i] = grid.grid[i];

	for(int y = 1; y < (c::L - 1); ++y)
	{
		for(int x = 1; x < (c::K - 1); ++x)
		{
			for(int z = 1; z < (c::M - 1); ++z)
			{
				// co to sa state values:
				// state values such as the temperature,
				// velocity and density of the clouds (...)

				// The state values at the lattice point (x, y, z) are
				// transported to a new position
				// (x + v_x, y + v_y, z + v_z) = (l + d_l, m + d_m, n + d_n)

				//if(grid(x, y, z).velocity.x >= 1.0f || grid(x, y, z).velocity.y >= 1.0f || grid(x, y, z).velocity.z >= 1.0f)
				//	std::cout << grid(x, y, z).velocity.x << ", " << grid(x, y, z).velocity.y << ", " << grid(x, y, z).velocity.z << " !!! \n";

				// wartosci (state values) w punkcie (x, y, z) sa przenoszone
				// zgodnie z polem predkosci w nowe punkty
				auto & grid_velocity = grid(x, y, z).velocity;
				//if(grid_velocity.x >= 1.0f)
				//	grid_velocity.x = 0.99f;
				//else if(grid_velocity.x <= -1.0f)
				//	grid_velocity.x = -0.99f;
				//if(grid_velocity.y >= 1.0f)
				//	grid_velocity.y = 0.99f;
				//else if(grid_velocity.y <= -1.0f)
				//	grid_velocity.y = -0.99f;
				//if(grid_velocity.z >= 1.0f)
				//	grid_velocity.z = 0.99f;
				//else if(grid_velocity.z <= -1.0f)
				//	grid_velocity.z = -0.99f;

				glm::vec3 new_position = glm::vec3(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)) + grid(x, y, z).velocity;
				float fl, fm, fn;

				// d_* to czesci po przecinku wazace 
				// l, m, n to nowe pozycje punktu znajdujacego si� w x, y, z
				auto d_l = modff(new_position.x, &fl);
				auto d_m = modff(new_position.y, &fm);
				auto d_n = modff(new_position.z, &fn);
				auto d_lmn = glm::vec3(d_l, d_m, d_n);

				auto dir_of_change_l = grid_velocity.x >= 0.0f ? 1 : -1;
				auto dir_of_change_m = grid_velocity.y >= 0.0f ? 1 : -1;
				auto dir_of_change_n = grid_velocity.z >= 0.0f ? 1 : -1;

				if(dir_of_change_l < 0.0f)
					d_l = 1.0f - d_l;
				if(dir_of_change_m < 0.0f)
					d_m = 1.0f - d_m;
				if(dir_of_change_n < 0.0f)
					d_n = 1.0f - d_n;

				// It is possible that the simulation is unstable if the
				// following condition is not satisfied.
				// v_x, v_y, v_z, < 1.0 !!!
				// ten warunek chyba mo�na zachowa�
				// ustawiaj�c odpowiednio ma�y krok czasowy
				// (zeby zmiany polozenia w jednym kroku czasowym nie byly tak duze)
				// czyli l, m i n powinny by� zawsze: 1 lub -1
				auto l = static_cast<int>(fl);
				auto m = static_cast<int>(fm);
				auto n = static_cast<int>(fn);

				// pomijam brzegi
				if(l <= 0 || m <= 0 || n <= 0 || l > c::K - 2 || m > c::L - 2 || n > c::M - 2)
					continue;

				// The values are then distributed to the adjacent lattices.
				// State values at adjacent lattices of (l, m, n) lattice point:
				Point & sv_lmn = advected_grid_point(l, m, n);
				Point & sv_l1mn = advected_grid_point(l + dir_of_change_l, m, n);
				Point & sv_lm1n = advected_grid_point(l, m + dir_of_change_m, n);
				Point & sv_l1m1n = advected_grid_point(l + dir_of_change_l, m + dir_of_change_m, n); // l+1, m+1, n
				Point & sv_lmn1 = advected_grid_point(l, m, n + dir_of_change_n);
				Point & sv_l1mn1 = advected_grid_point(l + dir_of_change_l, m, n + dir_of_change_n);
				Point & sv_lm1n1 = advected_grid_point(l, m + dir_of_change_m, n + dir_of_change_n);
				Point & sv_l1m1n1 = advected_grid_point(l + dir_of_change_l, m + dir_of_change_m, n + dir_of_change_n); // l+1, m+1, n+1

				// musze odjac od punktu (x, y, z) tyle ile
				// odplynie 'masy' przez adwekcje do innych punktow
				auto summary_advected_mass = Point();
				
				// mnoze kazda ze state value przez wagi d_* (state value weights: svw_*)
				auto svw_lmn = (1.0f - d_l)*(1.0f - d_m)*(1.0f - d_n);
				auto p_svw_lmn = Point();
				p_svw_lmn.droplets = grid(x, y, z).droplets * svw_lmn;
				p_svw_lmn.temperature = grid(x, y, z).temperature * svw_lmn;
				p_svw_lmn.vapor = grid(x, y, z).vapor * svw_lmn;
				p_svw_lmn.velocity = grid(x, y, z).velocity * svw_lmn;

				sv_lmn.droplets += p_svw_lmn.droplets;
				sv_lmn.temperature += p_svw_lmn.temperature;
				sv_lmn.vapor += p_svw_lmn.vapor;
				sv_lmn.velocity += p_svw_lmn.velocity;

				summary_advected_mass.droplets += p_svw_lmn.droplets;
				summary_advected_mass.temperature += p_svw_lmn.temperature;
				summary_advected_mass.vapor += p_svw_lmn.vapor;
				summary_advected_mass.velocity += p_svw_lmn.velocity;
				
				
				auto svw_l1mn = (d_l)*(1.0f - d_m)*(1.0f - d_n);
				auto p_svw_l1mn = Point();
				p_svw_l1mn.droplets = grid(x, y, z).droplets * svw_l1mn;
				p_svw_l1mn.temperature = grid(x, y, z).temperature * svw_l1mn;
				p_svw_l1mn.vapor = grid(x, y, z).vapor * svw_l1mn;
				p_svw_l1mn.velocity = grid(x, y, z).velocity * svw_l1mn;

				sv_l1mn.droplets += p_svw_l1mn.droplets;
				sv_l1mn.temperature += p_svw_l1mn.temperature;
				sv_l1mn.vapor += p_svw_l1mn.vapor;
				sv_l1mn.velocity += p_svw_l1mn.velocity;

				summary_advected_mass.droplets += p_svw_l1mn.droplets;
				summary_advected_mass.temperature += p_svw_l1mn.temperature;
				summary_advected_mass.vapor += p_svw_l1mn.vapor;
				summary_advected_mass.velocity += p_svw_l1mn.velocity;


				auto svw_lm1n = (1.0f - d_l)*(d_m)*(1.0f - d_n);
				auto p_svw_lm1n = Point();
				p_svw_lm1n.droplets = grid(x, y, z).droplets * svw_lm1n;
				p_svw_lm1n.temperature = grid(x, y, z).temperature * svw_lm1n;
				p_svw_lm1n.vapor = grid(x, y, z).vapor * svw_lm1n;
				p_svw_lm1n.velocity = grid(x, y, z).velocity * svw_lm1n;

				sv_lm1n.droplets += p_svw_lm1n.droplets;
				sv_lm1n.temperature += p_svw_lm1n.temperature;
				sv_lm1n.vapor += p_svw_lm1n.vapor;
				sv_lm1n.velocity += p_svw_lm1n.velocity;

				summary_advected_mass.droplets += p_svw_lm1n.droplets;
				summary_advected_mass.temperature += p_svw_lm1n.temperature;
				summary_advected_mass.vapor += p_svw_lm1n.vapor;
				summary_advected_mass.velocity += p_svw_lm1n.velocity;


				auto svw_l1m1n = (d_l)*(d_m)*(1.0f - d_n);
				auto p_svw_l1m1n = Point();
				p_svw_l1m1n.droplets = grid(x, y, z).droplets * svw_l1m1n;
				p_svw_l1m1n.temperature = grid(x, y, z).temperature * svw_l1m1n;
				p_svw_l1m1n.vapor = grid(x, y, z).vapor * svw_l1m1n;
				p_svw_l1m1n.velocity = grid(x, y, z).velocity * svw_l1m1n;

				sv_l1m1n.droplets += p_svw_l1m1n.droplets;
				sv_l1m1n.temperature += p_svw_l1m1n.temperature;
				sv_l1m1n.vapor += p_svw_l1m1n.vapor;
				sv_l1m1n.velocity += p_svw_l1m1n.velocity;

				summary_advected_mass.droplets += p_svw_l1m1n.droplets;
				summary_advected_mass.temperature += p_svw_l1m1n.temperature;
				summary_advected_mass.vapor += p_svw_l1m1n.vapor;
				summary_advected_mass.velocity += p_svw_l1m1n.velocity;

				
				auto svw_lmn1 = (1.0f - d_l)*(1.0f - d_m)*(d_n);
				auto p_svw_lmn1 = Point();
				p_svw_lmn1.droplets = grid(x, y, z).droplets * svw_lmn1;
				p_svw_lmn1.temperature = grid(x, y, z).temperature * svw_lmn1;
				p_svw_lmn1.vapor = grid(x, y, z).vapor * svw_lmn1;
				p_svw_lmn1.velocity = grid(x, y, z).velocity * svw_lmn1;

				sv_lmn1.droplets += p_svw_lmn1.droplets;
				sv_lmn1.temperature += p_svw_lmn1.temperature;
				sv_lmn1.vapor += p_svw_lmn1.vapor;
				sv_lmn1.velocity += p_svw_lmn1.velocity;

				summary_advected_mass.droplets += p_svw_lmn1.droplets;
				summary_advected_mass.temperature += p_svw_lmn1.temperature;
				summary_advected_mass.vapor += p_svw_lmn1.vapor;
				summary_advected_mass.velocity += p_svw_lmn1.velocity;


				auto svw_l1mn1 = (d_l)*(1.0f - d_m)*(d_n);
				auto p_svw_l1mn1 = Point();
				p_svw_l1mn1.droplets = grid(x, y, z).droplets * svw_l1mn1;
				p_svw_l1mn1.temperature = grid(x, y, z).temperature * svw_l1mn1;
				p_svw_l1mn1.vapor = grid(x, y, z).vapor * svw_l1mn1;
				p_svw_l1mn1.velocity = grid(x, y, z).velocity * svw_l1mn1;

				sv_l1mn1.droplets += p_svw_l1mn1.droplets;
				sv_l1mn1.temperature += p_svw_l1mn1.temperature;
				sv_l1mn1.vapor += p_svw_l1mn1.vapor;
				sv_l1mn1.velocity += p_svw_l1mn1.velocity;

				summary_advected_mass.droplets += p_svw_l1mn1.droplets;
				summary_advected_mass.temperature += p_svw_l1mn1.temperature;
				summary_advected_mass.vapor += p_svw_l1mn1.vapor;
				summary_advected_mass.velocity += p_svw_l1mn1.velocity;


				auto svw_lm1n1 = (1.0f - d_l)*(d_m)*(d_n);
				auto p_svw_lm1n1 = Point();
				p_svw_lm1n1.droplets = grid(x, y, z).droplets * svw_lm1n1;
				p_svw_lm1n1.temperature = grid(x, y, z).temperature * svw_lm1n1;
				p_svw_lm1n1.vapor = grid(x, y, z).vapor * svw_lm1n1;
				p_svw_lm1n1.velocity = grid(x, y, z).velocity * svw_lm1n1;

				sv_lm1n1.droplets += p_svw_lm1n1.droplets;
				sv_lm1n1.temperature += p_svw_lm1n1.temperature;
				sv_lm1n1.vapor += p_svw_lm1n1.vapor;
				sv_lm1n1.velocity += p_svw_lm1n1.velocity;

				summary_advected_mass.droplets += p_svw_lm1n1.droplets;
				summary_advected_mass.temperature += p_svw_lm1n1.temperature;
				summary_advected_mass.vapor += p_svw_lm1n1.vapor;
				summary_advected_mass.velocity += p_svw_lm1n1.velocity;


				auto svw_l1m1n1 = (d_l)*(d_m)*(d_n);
				auto p_svw_l1m1n1 = Point();
				p_svw_l1m1n1.droplets = grid(x, y, z).droplets * svw_l1m1n1;
				p_svw_l1m1n1.temperature = grid(x, y, z).temperature * svw_l1m1n1;
				p_svw_l1m1n1.vapor = grid(x, y, z).vapor * svw_l1m1n1;
				p_svw_l1m1n1.velocity = grid(x, y, z).velocity * svw_l1m1n1;

				sv_l1m1n1.droplets += p_svw_l1m1n1.droplets;
				sv_l1m1n1.temperature += p_svw_l1m1n1.temperature;
				sv_l1m1n1.vapor += p_svw_l1m1n1.vapor;
				sv_l1m1n1.velocity += p_svw_l1m1n1.velocity;

				summary_advected_mass.droplets += p_svw_l1m1n1.droplets;
				summary_advected_mass.temperature += p_svw_l1m1n1.temperature;
				summary_advected_mass.vapor += p_svw_l1m1n1.vapor;
				summary_advected_mass.velocity += p_svw_l1m1n1.velocity;
				
				auto g_point = grid(x, y, z);

				//ta roznica nie moze byc < 0
				//assert((grid(x, y, z).droplets - summary_advected_mass.droplets) >= -0.001f);

				advected_grid_point(x, y, z) = advected_grid_point(x, y, z) - summary_advected_mass;
			}
		}
	}

	auto velocity_grid = std::vector<glm::vec3>();
	velocity_grid.resize(c::KLM);

	for(long i = 0; i < c::KLM; ++i)
		velocity_grid[i] = advected_grid[i].velocity;

	set_velocity_boundary(velocity_grid);
	set_periodic_boundary<decltype(advected_grid), Point>(advected_grid);

	for(long i = 0; i < c::KLM; ++i)
		advected_grid[i].velocity = velocity_grid[i];

	for(long i = 0; i < c::KLM; ++i)
		grid.grid[i] = advected_grid[i];
}

void Simulation::diffusion_of_water_vapor()
{
	auto vapor_grid = std::vector<float>();
	vapor_grid.resize(c::KLM);
	
	for(int i = 0; i < c::KLM; ++i)
		vapor_grid[i] = grid.grid[i].vapor;


	for(int z = 1; z < (c::M - 1); ++z)
	{
		for(int y = 1; y < (c::L - 1); ++y)
		{
			for(int x = 1; x < (c::K - 1); ++x)
			{
				// eqation (6): w_v(x,y,z)
				auto part1 = grid(x, y, z).vapor;

				// equation (6): delta*w(x,y,z)
				auto delta_vapor = (1.0f / 6.0f)*(grid(x + 1, y, z).vapor + grid(x - 1, y, z).vapor +
					grid(x, y + 1, z).vapor + grid(x, y - 1, z).vapor +
					grid(x, y, z + 1).vapor + grid(x, y, z - 1).vapor +
					(-6.0f)*grid(x, y, z).vapor);

				auto part2 = c::coefficient_vapor * delta_vapor;
				auto new_vapor = part1 + part2;

				vapor_grid[(x + y * c::K + z * c::KL)] = new_vapor;
			}
		}
	}

	set_periodic_boundary<decltype(vapor_grid), float>(vapor_grid);

	for(int i = 0; i < c::KLM; ++i)
		grid.grid[i].vapor = vapor_grid[i];
}

void Simulation::thermal_diffusion()
{
	auto temperature_grid = std::vector<float>();
	temperature_grid.resize(c::KLM);

	for(int i = 0; i < c::KLM; ++i)
		temperature_grid[i] = grid.grid[i].temperature;

	for(int z = 1; z < (c::M - 1); ++z)
	{
		for(int y = 1; y < (c::L - 1); ++y)
		{
			for(int x = 1; x < (c::K - 1); ++x)
			{
				// eqation (7): E(x,y,z)
				auto part1 = grid(x, y, z).temperature;

				// equation (6): delta*E(x,y,z)
				auto delta_temperature = (1.0f / 6.0f)*(grid(x + 1, y, z).temperature + grid(x - 1, y, z).temperature +
					grid(x, y + 1, z).temperature + grid(x, y - 1, z).temperature +
					grid(x, y, z + 1).temperature + grid(x, y, z - 1).temperature +
					(-6.0f)*grid(x, y, z).temperature);


				auto part2 = c::coefficient_temperature * delta_temperature;
				auto new_temperature = part1 + part2;

				temperature_grid[(x + y * c::K + z * c::KL)] = new_temperature;
			}
		}
	}

	set_periodic_boundary<decltype(temperature_grid), float>(temperature_grid);

	for(int i = 0; i < c::KLM; ++i)
		grid.grid[i].temperature = temperature_grid[i];
}

void Simulation::buoyancy()
{
	auto buoyancy_grid = std::vector<float>();
	buoyancy_grid.resize(c::KLM);

	auto grid_point = [=, &buoyancy_grid](int x, int y, int z) -> float&
	{
		return buoyancy_grid[(static_cast<long>(x) + static_cast<long>(y) * static_cast<long>(c::K) + static_cast<long>(z) * static_cast<long>(c::KL))];
	};

	for(int i = 0; i < c::KLM; ++i)
		buoyancy_grid[i] = grid.grid[i].velocity.y;

	for(int y = 1; y < (c::L - 1); ++y)
	{
		for(int z = 1; z < (c::M - 1); ++z)
		{
			for(int x = 1; x < (c::K - 1); ++x)
			{
				// V_y
				auto part1 = grid(x, y, z).velocity.y;
				auto part2 = (0.25f * c::coefficient_buoyancy) * (4.0f * grid(x, y, z).temperature - 
					grid(x + 1, y, z).temperature - grid(x - 1, y, z).temperature - 
					grid(x, y, z + 1).temperature - grid(x, y, z - 1).temperature);

				auto velocityy = part1 + part2;

				//if(velocityy >= 1.0f)
				//	velocityy = 0.99f;
				//else if(velocityy <= -1.0f)
				//	velocityy = -0.99f;

				buoyancy_grid[(x + y * c::K + z * c::KL)] = velocityy;
			}
		}
	}


	// XZ
	for(int z = 0; z < c::M; ++z)
	{
		for(int x = 0; x < c::K; ++x)
		{
			auto y = 0;
			grid_point(x, y, z) = -grid_point(x, y + 1, z);
			y = c::L - 1;
			grid_point(x, y, z) = -grid_point(x, y - 1, z);
		}
	}

	// rogi
	grid_point(0, 0, 0) = (grid_point(1, 0, 0) + grid_point(0, 1, 0) + grid_point(0, 0, 1))*0.33f;
	grid_point(c::K - 1, 0, 0) = (grid_point(c::K - 2, 0, 0) + grid_point(c::K - 1, 1, 0) + grid_point(c::K - 1, 0, 1))*0.33f;
	grid_point(0, c::L - 1, 0) = (grid_point(1, c::L - 1, 0) + grid_point(0, c::L - 2, 0) + grid_point(0, c::L - 1, 1))*0.33f;
	grid_point(0, 0, c::M - 1) = (grid_point(1, 0, c::M - 1) + grid_point(0, 1, c::M - 1) + grid_point(0, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, 0) = (grid_point(c::K - 2, c::L - 1, 0) + grid_point(c::K - 1, c::L - 2, 0) + grid_point(c::K - 1, c::L - 1, 1))*0.33f;
	grid_point(0, c::L - 1, c::M - 1) = (grid_point(1, c::L - 1, c::M - 1) + grid_point(0, c::L - 2, c::M - 1) + grid_point(0, c::L - 1, c::M - 2))*0.33f;
	grid_point(c::K - 1, 0, c::M - 1) = (grid_point(c::K - 2, 0, c::M - 1) + grid_point(c::K - 1, 1, c::M - 1) + grid_point(c::K - 1, 0, c::M - 2))*0.33f;
	grid_point(c::K - 1, c::L - 1, c::M - 1) = (grid_point(c::K - 2, c::L - 1, c::M - 1) + grid_point(c::K - 1, c::L - 2, c::M - 1) + grid_point(c::K - 1, c::L - 1, c::M - 2))*0.33f;

	for(int i = 0; i < c::KLM; ++i)
		grid.grid[i].velocity.y = buoyancy_grid[i];
}


void Simulation::phase_transition()
{
	auto vapor_grid = std::vector<float>();
	auto droplets_grid = std::vector<float>();
	auto temperature_grid = std::vector<float>();
	vapor_grid.resize(c::KLM);
	droplets_grid.resize(c::KLM);
	temperature_grid.resize(c::KLM);
	
	for(int i = 0; i < c::KLM; ++i)
	{
		vapor_grid[i] = grid.grid[i].vapor;
		droplets_grid[i] = grid.grid[i].droplets;
		temperature_grid[i] = grid.grid[i].temperature;
	}

	auto mass_sum = 0.0f;

	for(int y = 1; y < (c::L - 1); ++y)
	{
		for(int x = 1; x < (c::K - 1); ++x)
		{
			for(int z = 1; z < (c::M - 1); ++z)
			
			{
				auto idx = (x + y * c::K + z * c::KL);
				auto temperature_point = grid(x, y, z).temperature;
				//auto w_max = 217.0f*std::expf( (19.482f - 4303.4f / (temperature_point - 29.5f)) ) / temperature_point;
				auto w_max = 2.0f;//1e+5f

				droplets_grid[idx] += c::phase_transition_rate * (grid(x, y, z).vapor-w_max);
		  		vapor_grid[idx] -= c::phase_transition_rate * (grid(x, y, z).vapor-w_max);
		  		temperature_grid[idx] -= c::latent_heat * (grid(x, y, z).vapor-w_max);

				// masa powinna byc zachowana co kazda iteracje
				mass_sum += droplets_grid[idx] + vapor_grid[idx];
				//if(z == x)
					//std::cout << "w_max: " << w_max << "\n";
					//std::cout << x << ", " << y << ", " << z << "vapor-w_max: " << grid(x, y, z).vapor - w_max << "\ttemperature_point" << temperature_point << "\n";
			}
		}
	}

	set_periodic_boundary<decltype(vapor_grid), float>(vapor_grid);
	set_periodic_boundary<decltype(droplets_grid), float>(droplets_grid);
	set_periodic_boundary<decltype(temperature_grid), float>(temperature_grid);

	//std::cout << "mass_sum: " << mass_sum << "\n";

	for(int i = 0; i < c::KLM; ++i)
	{
		grid.grid[i].vapor = vapor_grid[i];
		grid.grid[i].droplets = droplets_grid[i];
		grid.grid[i].temperature = temperature_grid[i];
	}

}