# Fluid simulator for cloud modeling
## Modeling clouds using CML method.
The simulator is based on paper: "A Method for Modeling Clouds based on Atmospheric Fluid Dynamics" by R. Miyazaki et al. (see reference 1.).

Unfortunately we failed to recreate all phenomenons described in the paper. Clouds develop in somewhat chaotic manner and we were unable to fully control the formation process.


## Dependencies
- [GLM](http://glm.g-truc.net/0.9.8/index.html)
- [GLEW](http://glew.sourceforge.net/)
- [GLFW](http://www.glfw.org/)
- [SOIL](http://www.lonesock.net/soil.html)
- [libnoise](http://libnoise.sourceforge.net/)

Code compiles with Visual Studio 2013/2015; proper project file is in this repository (cloud-simulation.vcxproj).

## Introduction to the code
All simulating takes place in `Simulation.cpp`, in `Simulation::run()` method. Briefly:

```c++
// simulation loop
void Simulation::run()
{
    // calculate all forces
    buoyancy();
    // add vapor into simulation from sources (water reservoirs)
    add_water_vapor();
    viscosity_and_pressure_effect();
    thermal_diffusion();
    diffusion_of_water_vapor();
    phase_transition();
    // update 'state values' and transport them accordingly; update velocity
    advection();

    // update data for visualization (raycasting)
    volume_raycaster.update_volume_data(grid.grid);
}
```

Clouds are rendered using a raycasting method. A 3D texture containing all cloud data is updated after every iteration and then sampled with rays in fragment shader (see shaders/raycaster.frag).

## Video
[![Cloud](https://img.youtube.com/vi/ZoqjLTZE49c/0.jpg)](https://youtu.be/ZoqjLTZE49c?list=PL4G4UXbD6DRJi__Mj4UsOdLTGddWOy2YC)


## References
1. R. Miyazaki, S. Yoshida, Y. Dobashi, T. Nishita.
["A method for modeling clouds based on atmospheric fluid dynamics"](http://nishitalab.org/user/nis/cdrom/pg/PG2001_ryomiya.pdf), 2001
2. T. Yanagita, K. Kaneko. ["Modeling and Characterization of Cloud Dynamics"](http://chaos.c.u-tokyo.ac.jp/papers/pr/yanagita97.pdf), 1997
2. J. Stam. ["Real-Time Fluid Dynamics for Games"](http://www.intpowertechcorp.com/GDC03.pdf), 2003